﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace calculator_server
{
    public class math_problem
    {
        public double operand1;
        public double operand2;
        public char action;
        public int using_result; //if the problem uses a result of a different problem the value would be the operand that will be replaced by the result, if not the value will be 0
    }
}
