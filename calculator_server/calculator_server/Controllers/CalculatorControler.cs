﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace calculator_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalculatorController : ControllerBase
    {
        [HttpGet("/operations")]
        public IActionResult GetOperations()
        {
            var operations = new string[] { "+", "-", "*", "/" };
            return Ok(operations);
        }

        [HttpPost("/calculate")]
        public IActionResult Calculate([FromBody] CalculationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = EvaluateExpression(request.MathQuestion);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error calculating math question: {ex.Message}");
            }
        }

        private double EvaluateExpression(string mathQuestion)
        {
            double res = 0;
            //sort operations
            math_problem[] operations = sort_problems(mathQuestion);//result of sort function
            //solving every problem seperately
            for(int i = 0; i < operations.Length; i++)
            {
                if (operations[i].using_result == 1)
                {
                    operations[i].operand1 = res;
                }
                else if (operations[i].using_result == 2)
                {
                    operations[i].operand2 = res;
                }   

                res = solve(operations[i]);
            }
            return res;
        }

        private double solve(math_problem prob)
        {
            switch (prob.action)
            {
                case '+':
                    return prob.operand1 + prob.operand2;
                case '-':
                    return prob.operand1 - prob.operand2;
                case '*':
                    return prob.operand1 * prob.operand2;
                case '/':
                    if(prob.operand2 == 0)
                    {
                        throw new Exception("cannot divide by 0");
                    }
                    return prob.operand1 / prob.operand2;
                default:
                    throw new Exception("unkown operator");
            }
        }

        private math_problem[] sort_problems(string mathQuestion)
        {
            string operations = "+-*/";
            int sum_of_operations = 0, letter = 0;
            //counting the operators
            for (int i = 0; i < mathQuestion.Length; i++)
            {
                if(operations.Contains(mathQuestion[i]))
                {
                    sum_of_operations++;
                }
            }

            math_problem[] all_problems = new math_problem[sum_of_operations];

            for(int i = 0; i < sum_of_operations; i++)
            {
                math_problem current_problem = new math_problem();
                if(i > 0)
                {
                    //currently the order isnt based on mathematic order, so every problem except the first use the result of the previous problem
                    current_problem.using_result = 1;
                }

                //go through the problems until you find an operator
                letter = 0;
                while(!operations.Contains(mathQuestion[letter]))
                {
                    letter++;
                }

                current_problem.action = mathQuestion[letter];
                current_problem.operand1 = Convert.ToDouble(mathQuestion.Substring(0, letter));

                //getting rid of the used data to read the rest of the question
                mathQuestion = mathQuestion.Substring(letter + 1);
                letter = 0;

                //if this operation is the last operation all that's left of the data is the second operand
                if(i == sum_of_operations - 1)
                {
                    current_problem.operand2 = Convert.ToDouble(mathQuestion);
                }
                //if this is not the last operation, find the next operator and get the operand before it
                else
                {
                    letter = 0;
                    while (!operations.Contains(mathQuestion[letter]))
                    {
                        letter++;
                    }

                    current_problem.operand2 = Convert.ToDouble(mathQuestion.Substring(0, letter));
                }

                all_problems[i] = current_problem;
            }
            return all_problems;
        }
    }

    public class CalculationRequest
    {
        public string MathQuestion { get; set; }
    }
}
